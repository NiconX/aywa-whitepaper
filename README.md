# Aywa: Decentralized Communication and Rewards Platform #
###Nicon Xenakis ###
####Aywa SMSG Contact: AYWAx18HqkjdcEPvtxBodUWcZbtDuEUEYH (pubkey:rYVMd2r3Rfosh6sdw93bjGKm1SUTGE1nAiMsRG4Gis3f) 
Abstract. A cryptoplatform based on Bitcoin (Satoshi Nakamoto) and Dash (Evan Duffield and Daniel Diaz), with various improvements such as instant messages, based on BitMesssage protocol. Included are other improvements such as Time Gravity Wave, to provide stable time for rewards. Built-in SmartMining system allows easy to post an idea or proposal and gets rewards on the next day. The platform uses perfect Dash features as Masternode network, Private Send and Instant Send, which allows real instant transaction.#### 

##1 Introduction##
The advent of the blockchain era occurred in 2009 with its implementation in Bitcoin by the entity known as Satoshi Nakamoto. Following Bitcoin’s success, many competing cryptocurrencies—known as altcoins—have arisen. The potential of blockchain to revolutionize not only the way transactions are made, but the way business is conducted across many strata, has seen an explosion of interest in the technology. Currently, the cryptocurrency market is awash with tokens from parties of varying intent, motivation, and affiliation. The myriad of tokens and projects—some novel and ambitious uses of blockchain, others in essence clones with catchy names—serves as a deterrent to widespread adoption of crypto as a legitimate, borderless alternative to fiat currency.
Bitcoin, despite its constant innovation, has so far failed to be widely accepted and adopted as a currency, and remains widely viewed as a store of value rather than means of conducting everyday business. As the world approaches a decade since the launch of Bitcoin, a definitive identity for cryptocurrencies has yet to emerge. This lack of identity has caused the public to view the crypto marketplace as a stock market 2.0. Its volatility and saturation intimidate potential adopters, who regard it not as an alternative to fiat currencies, but as a risky investment opportunity.
In keeping with the spirit of cryptocurrency’s defining goal, Aywa aims to bridge the gap between the tech-savvy and tech-wary. It strives to provide a safe means through which not only investors, but the general public can conduct business without the need for financial institutions or middle-men. Aywa’s aim is to provide the people of the ever more interconnected world with an expedient, private means to conduct business on their own behalf.
Aywa is the first cryptoplatform based on the work of Satoshi Nakamoto with built-in SmartMining functions. In this paper we propose a series of improvements to Bitcoin resulting in a decentralized, strongly anonymous cryptocurrency, with tamper-proof instant transactions and a secondary peer-to-peer (P2P) network incentivized to provide services to the Aywa Network.

##2 Masternode Network##

Full nodes are servers running on a P2P network that allow peers to use them to receive updates about the events on the network. These nodes utilize significant amounts of traffic and other resources that incur a substantial cost. As a result, a steady decrease in the amount of these nodes has been observed for some time on the Bitcoin network and as a result, block propagation times have been upwards of 40 seconds. Many solutions have been proposed such as a new reward scheme by Microsoft Research and the Bitnodes incentive program.
These nodes are very important to the health of the network. They provide clients with the ability to synchronize and facilitate quick propagation of messages throughout the network. We propose adding a secondary network, known as the Dash masternode network. These nodes will have high availability and provide a required level of service to the network in order to take part in the Masternode Reward Program.
###2.1 Masternode Reward Program - Cost and Payments
Much of the reason for the decrease of full nodes on the Bitcoin network is the lack of incentive to run one. Over time, the cost of running a full node increases as the network gets used more, creating more bandwidth and costing the operator more money. As the cost rises, operators consolidate their services to be cheaper to run, or run a light client which does not help the network at all.
Masternodes are full nodes, just like in the Bitcoin network, except they must provide a level of service to the network and have a bond of collateral to participate. The collateral is never forfeit and is safe while the masternode is operating. This permits masternode operators to provide a service to the network, earn payment for their services and reduce the volatility of the currency.

To run a masternode, the operator must demonstrate control over 10,000 AYWA (from 01/01/2021). 
	
	Masternode cost will grow from the genesis block to 01/02/2021:
	₳1,000 - before 01/01/2019;
	₳2,000 - 01/01/2019-30/06/2019;
	₳5,000 - 01/06/2019-31/12/2020;
	₳10,000 - from 01/01/2021.
	
This allow to provide more decentralisation on Masternode and prevent Masternodes concentration at several owners, as it has happened with many well-known cryptocurrencies. 
When active, masternodes provide services to clients on the network, and in return receive regular payment from the block reward. Like miners, masternodes are all paid from the block reward, 25% of which is dedicated to this program.
Due to the fact that the masternode rewards program is a fixed percentage and the masternode network nodes are fluctuating, expected masternode rewards will vary according to the current total count of active masternodes. 

Payments for a standard day for running a masternode can be calculated by using the following formula:

	(n / t) * r * b * a
	Where:
	n is the number of masternodes an operator controls
	t is the total number of masternodes
	r is the current block reward (presently averaging about 100 AYWA)
	b is blocks in an average day.
	a is the average masternode payment (25% of the average block reward)

The cost associated with running a masternode creates a hard and soft limit of active nodes on the network. Currently with 300,000 AYWA in circulation, only 300 nodes could possibly be running on the network. The soft limit is imposed by the price it costs to acquire a node and the limited liquidity on exchanges due to usage of Aywa as a currency and not merely an investment.

###2.2 Deterministic Ordering###

A special deterministic algorithm, developed by Dash, is used to create a pseudo-random ordering of the masternodes. By using the hash from the proof-of-work for each block, security of this functionality will be provided by the mining network.

Pseudocode, for selecting a masternode:

	for(mastenode in masternodes)
		{
    	current_score = masternode.CalculateScore();      
    	if(current_score > best_score){
        	best_score = current_score;
        	winning_node = masternode;
    	}
	}
	CMasterNode::CalculateScore(){
   		pow_hash = GetProofOfWorkHash(nBlockHeight); // get the hash of this block
   		pow_hash_hash = Hash(pow_hash); //hash the POW hash to increase the entropy
   		difference = abs(pow_hash_hash - masternode_vin);
   		return difference;
	}
The example code can be extended further to provide rankings of masternodes also, a “second”, “third”, “fourth” masternode in the list to be selected.

###2.3 Trustless Quorums

Currently the Aywa network has ~50 active masternodes. By requiring 1,000 AYWA collateral to become an active masternode, we create a system in which no one can control the entire network of masternodes. For example, if someone wanted to control 50% of the masternode network, they would have to buy 25,000 AYWA from the open market. This would raise the price substantially and it would become impossible to acquire the needed AYWA.
With the addition of the masternode network and the collateral requirements, we can use this secondary network to do highly sensitive tasks in a trustless way, where no single entity can control the outcome. By selecting N pseudo random masternodes from the total pool to perform the same task, these nodes can act as an oracle, without having the whole network do the task.
As an example, implementation of a trustless quorum (see InstantSend), which uses quorums to approve transactions and lock the inputs or the proof-of-service implementation.
Another example use for trustless quorums can include utilizing the masternode network as a decentralized oracle for financial markets, making secure decentralized contracts a possibility. As an example contract, if Apple Stock (AAPL) is over $300 on Dec 31, 2018 pay public key A, otherwise pay public key B.
##2.4 Roles and Proof-Of-Service
Masternodes can provide any number of extra services to the network. As a proof-of-concept, our first implementation included PrivateSend and InstantSend. By utilizing what we call proof-of-service, we can require that these nodes are online, responding and even at the correct block height.
Bad actors could also run masternodes, but not provide any of the quality service that is required of the rest of the network. To reduce the possibility of people using the system to their advantage nodes must ping the rest of the network to ensure they remain active. This work is done by the masternode network by selecting 2 quorums per block. Quorum A checks the service of Quorum B each block. Quorum A are the closest nodes to the current block hash, while Quorum B are the furthest nodes from said hash.
Masternode A (1) checks Masternode B (rank 2300)
Masternode A (2) checks Masternode B (rank 2299)
Masternode A (3) checks Masternode B (rank 2298)
All work done to check the network to prove that nodes are active is done by the masternode network itself. Approximately 1% of the network will be checked each block. This results in the entire network being checked about six times per day. In order to keep this system trustless, we select nodes randomly via the Quorum system, then we also require a minimum of six violations in order to deactivate a node.
In order to trick this system, an attacker will need to be selected six times in a row. Otherwise, violations will be cancelled out by the system as other nodes are selected by the quorum system.


	Attacker Controlled Masternodes / Total Masternodes	Required Picked Times In A Row	Probability of success  	AYWA Required
	1/2300							6		6.75e-21		1,000 AYWA
	10/2300							6		6.75e-15		10,000 AYWA
	100/2300						6		6.75e-09		100,000 AYWA
	500/2300						6		0.01055%		500,000 AYWA
	1000/2300						6		0.6755%			1,000,000 AYWA

	n is the total number of nodes controlled by the attacker
	t is the total number of masternodes in the network
	r is the depth of the chain
	
	
The selection of masternodes is pseudo random based on the Quorum system.

##2.5 Masternode Protocol

The masternodes are propagated around the network using a series of protocol extensions including a masternode announce message and masternode ping message. These two messages are all that is needed to make a node active on the network, beyond these there are other messages for executing a proof-of-service request, PrivateSend and InstantSend.
Masternodes are originally formed by sending required exact quantity of AYWA (called masternode collateral) to a specific address in a wallet that will “activate” the node making it capable of being propagated across the network. A secondary private key is created that is used for signing all further messages. The latter key allows the wallet to be completely locked when running in a standalone mode.
A cold mode is made possible by utilizing the secondary private key on two separate machines. The primary “hot” client signs the masternode collateral input including the secondary signing private key in the message. Soon after the “cold” client sees a message including its secondary key and activates as a masternode. This allows the “hot” client to be deactivated (client turned off) and leaves no possibility of an attacker gaining access to the masternode collateral by gaining access to the masternode after activation.
Upon starting, a masternode sends a “Masternode Announce” message to the network, containing:
Message: (masternode collateral AYWA Input, Reachable IP Address, Signature, Signature Time, masternode collateral AYWA Public Key, Secondary Public Key, Donation Public Key, Donation Percentage)
Every 15 minutes thereafter, a ping message is sent proving the node is still alive.
Message: (masternode collateral AYWA Input, Signature (using secondary key), Signature Time, Stop)
After a time-to-live has expired the network will remove an inactive node from the network, causing the node to not be used by clients or paid. Nodes can also ping the network constantly, but if they do not have their ports open, they will eventually be flagged as inactive and not be paid.
##2.6 Propagation of the Masternode List
New clients entering the Dash network must be made aware of the currently active masternodes on the network to be able to utilize their services. As soon as they join the mesh network, a command is sent to their peers asking for the known list of masternodes. A cache object is used for clients to record masternodes and their current status, so when clients restart they will simply load this file rather than asking for the full list of masternodes.

##2.7 Payments via Mining and Enforcement

To ensure that each masternode is paid its fair share of the block reward, the network must enforce that blocks pay the correct masternode. If a miner is non-compliant their blocks must be rejected by the network, otherwise cheating will be incentivized.
We propose a strategy where masternodes form quorums, select a winning masternode and broadcast their message. After N messages have been broadcast to select the same target payee, a consensus will be formed and that block in question will be required to pay that masternode.
When mining on the network, pool software (websites that merge the efforts of individual miners) use the RPC API interface to get information about how to make a block. To pay the masternodes, this interface must be extended by adding a secondary payee to GetBlockTemplate. Pools then propagate their successfully mined blocks, with a split payment between themselves and a masternode.

#3 PrivateSend

We believe it is important to have a standard trustless implementation for improving the privacy of its users in the reference client that provides a high degree of privacy. Other clients such as Electrum, Android and iOS will also have the same anonymity layer implemented directly and utilize the protocol extensions. This allows users a common experience anonymizing funds using a well understood system.
PrivateSend is an improved and extended version of the CoinJoin, employed by Dash a series of improvements such as decentralization, strong anonymity by using a chaining approach, denominations and passive ahead-of-time mixing.
The greatest challenge when improving privacy and fungibility of a cryptocurrency is doing it in a way that does not obscure the entire blockchain. In Bitcoin based crypto currencies, one can tell which outputs are unspent and which are not, commonly called UTXO (unspent transaction output). This results in a public ledger that allows any user to act as guarantor of the integrity of transactions. The Bitcoin protocol is designed to function without the participation of trusted counterparties, and in their absence, it is critical that auditing capabilities remain readily accessible to the users through the public blockchain. Our goal is to improve privacy and fungibility without losing these key elements that we believe make a successful currency.
By having a decentralized mixing service within the currency we gain the ability to keep the currency itself perfectly fungible. Fungibility is an attribute of money, that dictates that all units of a currency should remain equal. When you receive money within a currency, it should not come with any history from the previous users of the currency or the users should have an easy way to disassociate themselves from that history, thus keeping all coins equal. At the same time, any user should be able to act as an auditor to guarantee the financial integrity of the public ledger without compromising others privacy.
To improve the fungibility and keep the integrity of the public blockchain, we propose using an ahead-of-time decentralized trustless mixing strategy. To be effective at keeping the currency fungible, this service is directly built into the currency, easy to use and safe for the average user.

##3.1 Tracing CoinJoin By Amounts

A common strategy in existing Bitcoin implementations of CoinJoin is simply merging transactions together. This exposes the users to various methods of following the the users coins through these joined transaction.  
In this transaction, 0.05BTC was sent through the mixer. To identify the source of the money, one simply has to add up the values on the right until they match one of the values on the left.
	
	Breaking apart the transaction:
	•	0.05 + 0.0499 + 0.0001(fee) = 0.10BTC.
	•	0.0499 + 0.05940182 + 0.0001(fee) = 0.10940182BTC.
	
This gets exponentially more difficult as more users are added to the mixer. However, these sessions can be retroactively de-anonymized at any point in the future.

##3.2 Through Linking and Forward Linking

In other proposed implementations of CoinJoin, it is possible that a user anonymizes money then eventually sends change from that transaction to an exchange or other entity that knows the users identity. This breaks the anonymity and allows the entity to walk backwards through that users transactions. This type of attack called “Forward Linking”.
In this example, Alice anonymizes 1.2 BTC, which goes to two outputs, 1 BTC and 0.2 BTC. She then spends 0.7 BTC from the 1 BTC output, receiving change of 0.3 BTC. That 0.3 BTC then goes to an identifiable source, confirming Alice also spent the 0.7 BTC in the prior transaction.
To identify the sender of the anonymous transaction, start at the “exchange” transaction and go backwards in the blockchain till you get to the “Alice sends 0.7 BTC anonymously”. As the exchange, you know it was your user who just recently bought something anonymously, thus breaking the anonymity completely. We call this type of attack “Through Change Linking”.
In the second example, Alice buys 1.2 BTC from coinbase, then anonymizes this amount into a 1 BTC output. She then spends the 1 BTC, receives change in the amount of 0.3 BTC and then combines that with her 0.2 BTC earlier change.
By combining the change from the anonymous transaction (0.3 BTC) and the change she received from the CoinJoin transaction, you can link the entire history before and after, completely breaking the anonymity.

##3.3 Improved Privacy and Denial-of-Service (DOS) Resistance

PrivateSend uses the fact that a transaction can be formed by multiple parties and made out to multiple parties to merge funds together in a way where they cannot be uncoupled thereafter. Given that all PrivateSend transactions are setup for users to pay themselves, the system is highly secure against theft and users coins always remain safe. Currently, PrivateSend mixing requires at least three participants.
Users pay themselves back in the form of new outputs, which are randomly ordered.
To improve the privacy of the system as a whole we propose using common denominations of 0.1AYWA, 1AYWA, 10AYWA and 100AYWA. In each mixing session, all users should submit the same denominations as inputs and outputs. In addition to denominations, fees should be removed from the transactions and charged in bulk in separate, sporadic unlinkable transactions.
To address the possible DOS attacks, we propose all users submit a transaction as collateral to the pool when joining. This transaction will be made out to themselves and will pay a high fee to miners. In the case when a user submits a request to the mixing pool, they must provide collateral at the beginning of this exchange. If at any point any user fails to cooperate, by refusing to sign for example, the collateral transaction will be broadcast automatically. This will make it expensive to carry out a sustained attack on the privacy network.

##3.4 Passive Anonymization of funds and chaining

PrivateSend is limited to 1,000AYWA per session and requires multiple sessions to thoroughly anonymize significant amounts of money. To make the user experience easy and timing attacks very difficult, PrivateSend runs in a passive mode. At set intervals, a user’s client will request to join with other clients via a masternode. Upon entry into the masternode, a queue object is propagated throughout the network detailing the denominations the user is looking to anonymize, but no information that can be used to identify the user.
Each PrivateSend session can be thought of as an independent event increasing the anonymity of user’s funds. However each session is limited to three clients, so an observer has a one in three chance of being able to follow a transaction. To increase the quality of anonymity provided, a chaining approach is employed, which funds are sent through multiple masternodes, one after another.

	Depth Of The Chain		Possible Users  
	
	2						9
	4						81
	8						6561
	
	How many users could possibly be involved in N mixing sessions.
	
	

##3.5 Security Considerations

As transactions are merged, masternodes can possibly “snoop” on users funds as they pass through. This is not considered a serious limitation due to the requirement for masternode’s to hold 1,000AYWA and the fact that users utilize random masternodes that they select to host their joins. 

	The probability of following a transaction throughout a chaining event can be calculated as follows:
	
	Attacker Controlled Masternodes		Total Masternodes	Depth Of The Chain	Probability of success  	AYWA Required
	10									1010				2					9.80e-05					10,000AYWA
	10									1010				4					9.60e-09					10,000AYWA
	10									1010				8					9.51e-11					10,000AYWA
	100									1100				2					8.26e-03					100,000AYWA
	100									1100				4					6.83e-05					100,000AYWA
	100									1100				8					4.66e-09					100,000AYWA
	1000								2000				2					25%							1,000,000AYWA
	1000								2000				4					6.25%						1,000,000AYWA
	1000								2000				8					0.39%						1,000,000AYWA
	2000								3000				2					44.4%						2,000,000AYWA
	2000								3000				4					19.75%						2,000,000AYWA
	2000								3000				8					3.90%						2,000,000AYWA

	The probability of follow a PrivateSend transaction on the network given the attacker controls N Nodes.
	Where:
		n is the total number of nodes controlled by the attacker
		t is the total number of masternodes in the network
		r is the depth of the chain
		
		

The selection of masternodes is random.

Considering the limited supply of AYWA (0.2 million at the time of writing, October 2018) and the low liquidity available on the market, it becomes an impossibility to attain a large enough number of masternodes to succeed at such an attack.
Extending the system by blinding masternodes to the transactions taking place on their node will also greatly enhance the security of the system.

##3.6 Masternode Blinding via Relay System

In Section 3.4 we describe the probabilities of following a single transaction through multiple sessions of PrivateSend mixing. This can further be addressed by blinding masternodes, so they cannot see which inputs/outputs belong to which users. To do this we propose a simple relay system that users can use to protect their identity.
Instead of a user submitting the inputs and outputs directly into the pool, they will pick a random masternode from the network and request that it relays the inputs/outputs/signatures to the target masternode. This means that the masternode will receive N sets of inputs/outputs and N sets of signatures. Each set belongs to one of the users, but the masternode can’t know which belongs to which.

#4 Instant Transactions via InstantSend

By utilizing masternode quorums, users are able to send and receive instant irreversible transactions. Once a quorum has been formed, the inputs of the transaction are locked to only be spendable in a specific transaction, a transaction lock takes about four seconds to be set currently on the network. If consensus is reached on a lock by the masternode network, all conflicting transactions or conflicting blocks would be rejected thereafter, unless they matched the exact transaction ID of the lock in place.
This will allow vendors to use mobile devices in place of traditional POS systems for real world commerce and users to quickly settle face-to-face non commercial transactions as with traditional cash. This is done without a central authority. An extensive overview of this feature can be found in the InstantSend white paper.

#5 Additional Improvements

##5.1 Hashing algorithm

Aywa used widely used hashing algorithms, which takes a different approach, known as algorithm chaining. It consists of YesPower and some SHA3 contestants (Groestl and Keccak), each hash is calculated then submitted to the next algorithm in the chain. By utilizing multiple algorithms, the likelihood that an ASIC is created for the currency is minimal until a later part of its life cycle.
In the life cycle of Bitcoin, mining began with hobbyists which used Central Processing Units (CPUs) to mine the currency, then shortly after Graphics Processing Units (GPUs) software was created, which quickly replaced the CPUs. Years after the GPUs cycle, ASICs or Application Specific Integrated Circuits were created, which quickly replaced the GPUs.
YesPower based on Yescript, and its slow down GPUs, FPGAs and ASICs. It allows to keep mining from CPU as it was pland fot Bitcoin by Satoshi Nakamoto. 
Due to the complexity and die size required to create an ASIC to mine X11, we expect that it will take considerably longer than it did in Bitcoin, allowing for hobbyists to take part in the mining for a longer period of time. We believe this is highly important for good distribution and growth of a cryptocurrency.
Another benefit of the chaining hashing approach is high end CPUs give an average return similar to that of GPUs. Also GPUs have been reported to run 30-50% cooler, with less wattage than the Scrypt algorithm used by most current cryptocurrencies.

##5.2 Mining Supply

The well-tested Bitcoin approach to restricting the inflation of mining is taken in Aywa, using a halving of the supply every 4 year. In addition supply each block is directly tied to the amount of miners on the network; more miners result in lower mining rewards. Production of Aywa is scheduled to carry on throughout this century and onto the next, grinding down until finally near the year 2200, production will cease.
 
##5.3 Secure Messages and Public Channels (Chats)

Platform provides decentralised communications based on Bitmessage and Shadowcoin. This is not a blockchain system, but it not using any special servers. The messages encryption using similar method to bitmessage with the difference: bitmessage use curve sect283r1, coin addresses use secp256k1. 


##5.4 Idea Mining

The Decentralized Idea Mining Platform (DIMP) is an improved voting and funding Decentralized Governance by Blockchain from Dash. DIMP is developed for collaboration to improve the platform, such as ideas and/or its implementations. DIMP consists of three components: Proposals(Ideas), Votes, and Budgets. Anyone can submit a proposal for 1 AYWA directly from Aywa Core. Masternode owners can approve or decline votes on proposals. Approved proposals become budgets daily. Budgets are paid every day near 22:00 UTC directly from the blockchain to the address in proposal (usually proposal owner). The total daily budget payments cannot be more than 50% daily block rewards: 960*100*0.5 = 48,000 AYWA. If the approved proposals are exceed this maximum, budget for latest proposals will not be charged. Proposal is approved if voted more then 10 Masternodes and 50% + 1 votes are "YES". Every created Proposal (Idea) generates non-wallet address for its discussion (Public 
Channel). Every user of platform can subcribe and chat at this channel. Proposal owner subscribed to Proposal Channel automatically, if proposal was created in GUI Aywa-Qt Core.

 
##6 Conclusion

This paper introduces various concepts to improve the design of bitcoin resulting in improved privacy and fungibility for the average user, less price volatility and quicker message propagation throughout the network. This is all accomplished by utilizing an incentivized two-tier model, rather than the existing single-tier model in other cryptocurrencies such as Bitcoin. By utilizing this alternative network design it becomes possible to add many types of services such as decentralized mixing of coins, instant transactions and decentralized Idea Mining using masternode quorums.

Version 0.1, September 2018.
Written and compiled by NiconX

####References

1.	[A peer-to-peer electronic cash system (2008)](https://bitcoin.org/bitcoin.pdf)
2.	http://eprints.qut.edu.au/69169/1/Boyen_accepted_draft.pdf
3.	https://www.cryptocoinsnews.com/3-solutions-instant-bitcoin-confirmations/
4.	http://research.microsoft.com/pubs/156072/bitcoin.pdf
5.	http://www0.cs.ucl.ac.uk/staff/s.meiklejohn/files/imc13.pdf
6.	https://getaddr.bitnodes.io/nodes/incentive/
7.	https://medium.com/zapchain-magazine/why-don-t-people-run-bitcoin-nodes-anymore-d4da0b45aae5
8.	https://dashninja.pl/
9.	https://www.dash.org/wp-content/uploads/2014/09/InstantTX.pdf
10.	https://github.com/dashpay/dash/blob/master/src/Masternode-pos.cpp
11.	https://blockchain.info/tx/4eb3b2f9fe597d0aef6e43b58bbaa7b8fb727e645fa89f922952f3e57ee6d603
12.	https://blockchain.info/tx/1694122b34c8543d01ad422ce600d59f8d8fde495ac9ddd894edc7139aed7617
13.	http://en.wikipedia.org/wiki/NIST_hash_function_competition#Finalists
14.	http://www.tik.ee.ethz.ch/file/49318d3f56c1d525aabf7fda78b23fc0/P2P2013_041.pdf
15. https://www.openwall.com/yescrypt/
16. https://bitmessage.org

